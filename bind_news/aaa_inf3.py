


import time
import requests
import time
import random

from lxml import etree
from lxml.html import fromstring, tostring
from datetime import datetime
import json
import time
from bs4 import BeautifulSoup

import logging
import random
import re
from lxml import etree
import requests
from lxml import etree
from model.bus_information import InfoNewDao, BusInformation


class News1:
    def get_hrefs(self):
        headers = {
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7",
            "Accept-Language": "zh-CN,zh;q=0.9",
            "Cache-Control": "max-age=0",
            "Connection": "keep-alive",
            "Sec-Fetch-Dest": "document",
            "Sec-Fetch-Mode": "navigate",
            "Sec-Fetch-Site": "none",
            "Sec-Fetch-User": "?1",
            "Upgrade-Insecure-Requests": "1",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36",
            "sec-ch-ua": "\"Chromium\";v=\"116\", \"Not)A;Brand\";v=\"24\", \"Google Chrome\";v=\"116\"",
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": "\"Windows\""
        }

        # url = "https://www.okcis.cn/i3/"
        # response = requests.get(url, headers=headers)
        # response.encoding = 'gbk'
        # # print(response.text)
        # print(response)
        # for page in range(3, 10):
        url = "https://www.okcis.cn/php/new_site_center/infocenter/searchZixunlist.php"
        data = {
            "page": "1",
            "bidnoticekey": "3",  # 控制起始页数
            "result-keyword-or-form": "",
            "clicktime": "",
            "city-result-city-input%5B%5D": ""
        }
        response = requests.post(url, headers=headers, data=data)

        tree = etree.HTML(response.text)
        detail_url_list = tree.xpath('//h4/a[@class="hui01_a_20140520"]/@href')
        title_list = tree.xpath('//h4/a[@class="hui01_a_20140520"]/@title')

        print(detail_url_list)
        return detail_url_list, title_list

    def get_detail(self):
        l, t = self.get_hrefs()
        for index, detail_url in enumerate(l):
            print('detail_url === ', detail_url)

            headers = {
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7",
                "Accept-Language": "zh-CN,zh;q=0.9",
                "Cache-Control": "max-age=0",
                "Connection": "keep-alive",
                "Referer": "https://www.okcis.cn/i3/",
                "Sec-Fetch-Dest": "document",
                "Sec-Fetch-Mode": "navigate",
                "Sec-Fetch-Site": "same-origin",
                "Sec-Fetch-User": "?1",
                "Upgrade-Insecure-Requests": "1",
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36",
                "sec-ch-ua": "\"Chromium\";v=\"116\", \"Not)A;Brand\";v=\"24\", \"Google Chrome\";v=\"116\"",
                "sec-ch-ua-mobile": "?0",
                "sec-ch-ua-platform": "\"Windows\""
            }

            response = requests.get(detail_url, headers=headers)
            response.encoding = 'gbk'
            print(response.status_code)

            tree = etree.HTML(response.text)

            info = BusInformation()
            # 标题
            info.name = str(t[index]).replace('-招标采购导航网', '')
            # 图片
            info.image = ''
            # 状态：1=正常，2=禁用',
            info.status = 1
            # 简介
            try:
                info.describe = ''.join(tree.xpath('//div[@class="zxzx_nrm_20140922"]//text()')).replace(' ',
                                                                                                         '').replace(
                    '\n', '').replace('\t', '')[:120]
                print('info.describe123 === ', info.describe)
            except:
                info.describe = ''
            if info.describe == '':
                continue

            # 内容
            content = f'<head><meta charset="UTF-8"></head>' + str(
                tostring(tree.xpath('//div[@class="zxzx_nrm_20140922"]')[0],
                         encoding='UTF-8').decode('UTF-8'))
            info.content = content
            if info.content == '<head><meta charset="UTF-8"></head><p> </p>  ':
                continue

            # 发布时间
            date_str = tree.xpath('//li[@class="triqi_li02_20140923"]/span//text()')[0]
            date_format = "%Y-%m-%d"
            timestamp = datetime.strptime(date_str, date_format).timestamp()

            info.issue_time = int(timestamp)
            print('发布时间====', info.issue_time)

            # 点赞数
            info.like_num = 0
            # 查看人数
            info.view_num = 0

            InfoNewDao().add(info)
            time.sleep(2)

        pass

    def run(self):
        # self.get_hrefs()
        self.get_detail()


if __name__ == '__main__':
    NEW = News1()
    NEW.run()
