"""
采集：招标新闻
"""
from lxml.html import fromstring, tostring

import time
import datetime
import requests
from lxml import etree
from model.bus_information import InfoNewDao, BusInformation

"""
数据源：http://www.chinabidding.org.cn/RecommendEvents_id_1_ty_0.html 
"""


class News1:
    def get_hrefs(self):
        headers = {
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7",
            "Accept-Language": "zh-CN,zh;q=0.9",
            "Cache-Control": "max-age=0",
            "Connection": "keep-alive",
            "Upgrade-Insecure-Requests": "1",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36"
        }

        url = "http://www.chinabidding.org.cn/RecommendEvents_id_1_ty_0.html"
        response = requests.get(url, headers=headers, verify=False)
        print(response)

        tree = etree.HTML(response.text)

        detail_urls = ['http://www.chinabidding.org.cn/' + i for i in
                       tree.xpath("//table//tr/td[2]/table//tr//a/@href")]

        return detail_urls

    def get_detail(self):
        print("开始 进入 函数 ")
        detail_urls = self.get_hrefs()
        for index, detail_url in enumerate(detail_urls):
            print('detail_url === ', detail_url)

            headers = {
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7",
                "Accept-Language": "zh-CN,zh;q=0.9",
                "Cache-Control": "max-age=0",
                "Connection": "keep-alive",
                "Referer": "http://www.chinabidding.org.cn/RecommendEvents_id_1_ty_0.html",
                "Upgrade-Insecure-Requests": "1",
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36"
            }

            response = requests.get(detail_url, headers=headers, verify=False)

            if response.status_code != 200:
                print('请求出错 ===== ')
                break

            response.encoding = response.apparent_encoding

            tree = etree.HTML(response.text)

            info = BusInformation()
            # 标题
            info.name = tree.xpath('//div[@class="doutline"]/span[1]/text()')[0].replace(' ', '')
            # 图片
            info.image = ''
            # 状态：1=正常，2=禁用',
            info.status = 1
            # 简介
            try:
                info.describe = ''.join(tree.xpath('//div[@id="cnt"]//text()')).replace(' ',
                                                                                        '').replace(
                    '\n', '').replace('\t', '')[:200]
                print('info.describe123 === ', info.describe)
            except:
                info.describe = ''
            if info.describe == '':
                continue

            # 内容
            content = f'<head><meta charset="UTF-8"></head>' + str(
                tostring(tree.xpath('//div[@id="cnt"]')[0],
                         encoding='UTF-8').decode('UTF-8'))
            info.content = content
            if info.content == '<head><meta charset="UTF-8"></head><p> </p>  ':
                continue

            # 发布时间  2023年09月22日09:17
            date_str = tree.xpath('//span[@id="time"]//text()')[0].replace(' ', '').replace('\r', '').replace('\n',
                                                                                                              '').replace(
                "年", " ").replace("月",
                                  " ").replace(
                "日", " ")
            print('date_str ==== ', date_str)
            dt = datetime.datetime.strptime(date_str, '%Y %m %d %H:%M')

            info.issue_time = int(dt.timestamp())
            print('发布时间====', dt)

            # 点赞数
            info.like_num = 0
            # 查看人数
            info.view_num = 0

            InfoNewDao().add(info)
            time.sleep(2)

        pass

    def run(self):
        # self.get_hrefs()
        print('开始采集 ------------- ')
        self.get_detail()
        print('采集结束 ============= ')


if __name__ == '__main__':
    NEW = News1()
    NEW.run()
