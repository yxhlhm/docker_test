APScheduler==3.10.4
beautifulsoup4==4.11.1
lxml==4.9.2
PyMySQL==1.0.3
Requests==2.31.0
SQLAlchemy==1.4.39
