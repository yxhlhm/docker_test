# # -*- coding:utf-8 -*-
# import subprocess
#
# from apscheduler.schedulers.background import BackgroundScheduler
# from apscheduler.triggers.cron import CronTrigger
#
#
# def job_function():
#     # 获取当前 Python 解释器的路径
#     # python_executable = sys.executable  # 遇到报错可加上试试
#     """
#     定义要运行的多个 Python 程序的命令
#     "python script1.py arg1 arg2",  # 以示例参数运行 script1.py 带参数
#     """
#     commands = [
#         "python aaa_inf3.py",
#         "python aaa_inf4.py",
#     ]
#
#     # 遍历命令列表并执行每个命令
#     for command in commands:
#         try:
#             subprocess.run(command, shell=False, check=True)  # shell=False  为 True 在 win可能会出错 在centos就要为 True
#         except subprocess.CalledProcessError as e:
#             print(f"Error executing '{command}': {e}")
#
#
# # 创建一个后台调度程序
# scheduler = BackgroundScheduler()
#
# # 立即执行一次  测试 用
# job_function()
#
# # 启动调度程序
# scheduler.start()
#
# # 添加上午9点的任务
# # scheduler.add_job(job_function, CronTrigger.from_crontab('0 9 * * *'))
# #
# # # 添加下午2点的任务
# # scheduler.add_job(job_function, CronTrigger.from_crontab('0 14 * * *'))
#
#
# # 防止程序退出
# try:
#     while True:
#         pass
# except (KeyboardInterrupt, SystemExit):
#     # 当按下Ctrl+C或者程序退出时，关闭调度程序
#     scheduler.shutdown()


import subprocess
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.cron import CronTrigger


def job_function():
    commands = [
        "python /bind_news/aaa_inf3.py",  # 使用完整路径
        "python /bind_news/aaa_inf4.py",  # 使用完整路径
    ]

    for command in commands:
        try:
            subprocess.run(command, shell=True, check=True)  # 使用shell=True，因为我们在运行一个命令字符串
        except subprocess.CalledProcessError as e:
            print(f"Error executing '{command}': {e}")


scheduler = BackgroundScheduler()

# 立即执行一次测试
job_function()

# 启动调度程序
scheduler.start()

try:
    while True:
        pass
except (KeyboardInterrupt, SystemExit):
    scheduler.shutdown()
