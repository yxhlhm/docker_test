import json
import logging
import time
from sqlalchemy import func
from sqlalchemy.ext.declarative import declarative_base  # 调用sqlalchemy的基类
from sqlalchemy import Column, Index, distinct, update  # 指定字段属性，索引、唯一、DML
from sqlalchemy.types import *  # 所有字段类型
from . import DBSession, DBNullPoolSession
from . import Base


class BusInformation(Base):
    __tablename__ = 'bus_information'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(512), nullable=False, default='', comment='名称')
    image = Column(String(512), nullable=False, default='', comment='图片')
    status = Column(Integer, nullable=False, default=1, comment='状态：1=正常，2=禁用')
    content = Column(Text, nullable=False, comment='内容')
    describe = Column(String(1024), nullable=False, default='', comment='简介')
    issue_time = Column(Integer, nullable=False, default=0, comment='发布时间')
    like_num = Column(Integer, nullable=False, default=0, comment='点赞数')
    view_num = Column(Integer, nullable=False, default=0, comment='查看人数')
    create_time = Column(Integer, nullable=False, default=0, comment='创建时间')
    update_time = Column(Integer, nullable=False, default=0, comment='更新时间')

    def __init__(self):
        self.create_time = int(time.time())
        self.update_time = int(time.time())


class InfoNewDao(object):

    def add(self, info):
        sesion = DBSession()

        recruit_info = sesion.query(BusInformation).filter_by(name=info.name).first()
        if recruit_info:
            print("已存在 ===== ")
            sesion.close()
        else:
            sesion.add(info)
            sesion.commit()
            sesion.close()
