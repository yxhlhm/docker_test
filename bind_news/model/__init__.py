# encoding=UTF-8
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.pool import NullPool
import pymysql

from urllib.parse import quote_plus as urlquote


""" 
CREATE TABLE `bus_information` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '名称',
  `image` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '图片',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '状态：1=正常，2=禁用',
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '内容',
  `describe` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '简介',
  `issue_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '发布时间',
  `like_num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '点赞数',
  `view_num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '查看人数',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='资讯';
"""


# mysql
host = "ip "
port = 3306
user = "yxh"
password = ""
database = ""

url = f'mysql+pymysql://{user}:{urlquote(password)}@{host}:{port}/{database}?charset=utf8mb4'

Base = declarative_base()
# 初始化数据库连接:
engine = create_engine(
    # cfg.get("db"),
    url=url,
    echo=False,
    # pool_size=100,  # 连接个数
    pool_size=200,  # 连接个数
    pool_recycle=60 * 30,  # 不使用时断开
)

# 创建DBSession类型:
DBSession = sessionmaker(bind=engine)
# 创建不使用连接池的session
nullpoolengine = create_engine(
    # cfg.get("db"),
    url=url,
    echo=False,
    poolclass=NullPool
)
DBNullPoolSession = sessionmaker(bind=nullpoolengine)
